$(document).ready(function(){
    //pie chart
   const ctx_pie = document.getElementById('penyerapan-chart').getContext('2d');
   const data = {
    labels: ['PAGU', 'PENYERAPAN'],
    datasets: [
      {
        label: 'TOTAL PERSONIL',
        data: [99, 1],
        backgroundColor: ['#263053', '#ffc300']        
      }
    ]
  };
   const myChart2 = new Chart(ctx_pie, {
    type: 'doughnut',
    data: data,
    options: {
        aspectRatio: 1,
        responsive: true,
        plugins: {
        legend: {
            display: false,
            position: 'bottom',
        },
        title: {
            display: false,
            text: 'Chart.js Pie Chart'
        }
        }
    },
   });

   const ctx = document.getElementById('pagu-chart').getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['2018', '2019', '2020', '2021', '2022'],
                    datasets: [{
                        label: 'Pagu Anggaran ',
                        data: [1250, 1340, 1300, 1360, 1250],
                        backgroundColor: '#263053',
                        hoverBackgroundColor: '#263053',
                        borderColor: '#fff',
                        borderWidth: 1
                    },
                    {
                        label: 'Penyerapan ',
                        data: [1100, 1250, 1200, 1300, 20],
                        backgroundColor: '#ff9d50',
                        hoverBackgroundColor: '#ff9d50',
                        borderColor: '#fff',
                        borderWidth: 1
                    }]
                },
                options: {
                    aspectRatio: 3,
                    responsive: true,
                    plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart'
                    }
                    },
                    scales: {
                        x: {
                            grid: {
                            display: false
                            },
                            title: {
                            display: false
                            },
                        },
                        y: {
                            title: {
                            display: false
                            },
                            grid: {
                            display: true
                            },

                        }
                    },
                }
            });

            const ctx2 = document.getElementById('pagu-anggaran-chart').getContext('2d');
            const myChart3 = new Chart(ctx2, {
                type: 'bar',
                data: {
                    labels: ['Deputi 1', 'Deputi 2', 'Deputi 3', 'Deputi 4', 'Sekretariat Utama', 'Puskajibang', 'Pusdatik', 'Pusbang SDM', 'Poltek SSN', 'BSrE', 'Museum', 'BDS'],
                    datasets: [{
                        label: 'Pagu Anggaran ',
                        data: [300000000, 200000000000, 500000000, 500000000, 500000000000, 1200000000, 320000000000, 50000000000, 30000000000, 150000000000, 500000000, 1300000000],
                        backgroundColor: '#263053',
                        hoverBackgroundColor: '#263053',
                        borderColor: '#fff',
                        borderWidth: 1
                    },
                    {
                        label: 'Penyerapan ',
                        data: [24000000, 16000000000, 40000000, 40000000, 40000000000, 96000000, 25600000000, 4000000000, 2400000000, 12000000000, 40000000, 104000000],
                        backgroundColor: '#ff9d50',
                        hoverBackgroundColor: '#ff9d50',
                        borderColor: '#fff',
                        borderWidth: 1
                    }]
                },
                options: {
                    aspectRatio: 3,
                    responsive: true,
                    plugins: {
                    legend: {
                        display: false,
                        position: 'top',
                    },
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart'
                    }
                    },
                    scales: {
                        x: {
                            grid: {
                            display: false
                            },
                            title: {
                            display: false
                            },
                        },
                        y: {
                            title: {
                            display: false
                            },
                            grid: {
                            display: true
                            },

                        }
                    },
                }
            });
})