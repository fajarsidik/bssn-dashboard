$(document).ready(function(){

          //chart 1          
          const ctx = document.getElementById('chart3').getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Rusia', 'America', 'India', 'Arab', 'Singapore', 'Indonesia', 'Turki', 'London', 'China'],
                    datasets: [{
                        label: 'Serangan ',
                        data: [1200, 2190, 3000, 5000, 2122, 3019, 3019, 1019, 8019],
                        backgroundColor: '#b2b6c7',
                        hoverBackgroundColor: '#ff9d50',
                        borderColor: '#fff',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart'
                    }
                    },
                    scales: {
                        x: {
                            grid: {
                            display: false
                            },
                            title: {
                            display: false
                            },
                        },
                        y: {
                            title: {
                            display: false
                            },
                            grid: {
                            display: true
                            },

                        }
                    },
                }
            });

});