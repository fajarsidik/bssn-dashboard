$(document).ready(function(){

    // MAP
    var mymap = L.map('maps').setView([17.093911, 14.678468], 2);       
    
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZmFqYXJzaWRpeCIsImEiOiJja3YwOTdja2gyd3g5MndvZDBieWRhZWd6In0.G5umEESgvVMSCock5qjcgA', {
          attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 18,          
          zoom: 10,
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
          accessToken: 'pk.eyJ1IjoiZmFqYXJzaWRpeCIsImEiOiJja3YwOTdja2gyd3g5MndvZDBieWRhZWd6In0.G5umEESgvVMSCock5qjcgA'
        }).addTo(mymap);   
        
        var latLong = [
            {
              "location": "Nigeria",
              "lat": 18.216307,
              "lng": 11.884072,
              "date": "Desember 2021",
              "cases": "259729",
              "attackers": "China"
            },
            {
                "location": "Mozambique",
                "lat": -20.233852,
                "lng": 31.408819,
                "date": "Januari 2021",
                "cases": "159729",
                "attackers": "Rusia"
            },
            {
                "location": "India",
                "lat": 19.986104,
                "lng": 78.670715,
                "date": "Februari 2021",
                "cases": "199729",
                "attackers": "Canada"
            },
            {
                "location": "China",
                "lat": 36.777513,
                "lng": 98.719264,
                "date": "Februari 2021",
                "cases": "299729",
                "attackers": "Canada"
            },
            {
                "location": "Japan",
                "lat": 36.204823,
                "lng": 138.252930,
                "date": "Maret 2021",
                "cases": "99729",
                "attackers": "Canada"
            } ,  
            {
                "location": "Singapore",
                "lat": 0.936390,
                "lng": 104.227617,
                "date": "Juli 2021",
                "cases": "209729",
                "attackers": "Canada"
            }, 
            {
                "location": "Philippines",
                "lat": 12.713662,
                "lng": 122.664243,
                "date": "Juli 2021",
                "cases": "109729",
                "attackers": "China"
            }, 
            {
                "location": "USA",
                "lat": 38.320111,
                "lng": -99.952862,
                "date": "Juli 2021",
                "cases": "509729",
                "attackers": "China"
            }                                                      

          ]

          latLong.forEach(function (coord) {
            
            
            var radius = coord.cases;
            
  
            var circle = L.circle(coord, {
              color: '#ec2323',
              fillColor: '#ec2323',
              fillOpacity: 0.8,
              borderColor: 'red',
              weight: 10,
              opacity: 0.25,
              radius: radius,
            }).addTo(mymap);
  
            circle.bindPopup('<div class="marker_info"><div class="location">' + coord.location + '</div><p><span class="text-primary">' + coord.date + '</span></p> <p>Kasus: <span class="text-primary font-semibold">' + coord.cases + '</span></p> <p>Penyerang: <span class="font-semibold text-primary">' + coord.attackers + '</span></p></div>');
           
  
          });


        //chart 2        
        const ctx2 = document.getElementById('chart2').getContext('2d');
        const myChart2 = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: ['Rusia', 'America', 'India', 'Arab', 'Singapore', 'Indonesia', 'Turki', 'London', 'China'],
                datasets: [{
                    label: 'Serangan ',
                    data: [1200, 2190, 3000, 5000, 2122, 3019, 3019, 1019, 8019],
                    backgroundColor: '#b2b6c7',
                    hoverBackgroundColor: '#ff9d50',
                    borderColor: '#fbc143',
                    borderWidth: 4,
                    fill: {
                        target: 'origin',
                        above: 'rgba(253, 226, 120, 0.23)',   // Area will be red above the origin
                        below: 'rgba(253, 226, 120, 0.23)'    // And blue below the origin
                      }
                }]
            },
            options: {
               backgroundColor: '#000',
                pointBackgroundColor: '#fbc143',
                pointHoverBackgroundColor: '#ff0002',
                pointBorderColor: '#fff',
                pointBorderWidth: 2,
                radius: 7,
                interaction: {
                  intersect: false,
                },
                scales: {
                    x: {
                        grid: {
                        display: false
                        },
                        title: {
                        display: false
                        },
                    },
                    y: {
                        title: {
                        display: false
                        },
                        grid: {
                        display: true
                        },

                    }
                },
              },
        });
        
          
        //chart 3        
        const ctx = document.getElementById('chart3').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Rusia', 'America', 'India', 'Arab', 'Singapore', 'Indonesia', 'Turki', 'London', 'China'],
                datasets: [{
                    label: 'Serangan ',
                    data: [1200, 2190, 3000, 5000, 2122, 3019, 3019, 1019, 8019],
                    backgroundColor: '#263053',
                    hoverBackgroundColor: '#ff9d50',
                    borderColor: '#fff',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Chart.js Bar Chart'
                }
                },
                scales: {
                    x: {
                        grid: {
                        display: false
                        },
                        title: {
                        display: false
                        },
                    },
                    y: {
                        title: {
                        display: false
                        },
                        grid: {
                        display: true
                        },

                    }
                },
            }
        });        
});