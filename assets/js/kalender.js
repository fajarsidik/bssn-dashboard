document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
      initialView: 'dayGridMonth',
      initialDate: '2022-02-12',   
          
          editable: true,
          selectable: true,
          nowIndicator: true,
          dayMaxEvents: true, // allow "more" link when too many events        
          events: [
            {
              title: 'All Day Event',
              start: '2022-02-01'
            },
            {
              title: 'Long Event',
              start: '2022-02-01',
              end: '2022-02-04'
            }
          ]
    });
    calendar.render();
  });