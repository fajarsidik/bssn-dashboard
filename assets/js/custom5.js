$(document).ready(function(){

    // MAP
    var mymap = L.map('maps-cirst').setView([-2.1048555153693282, 118.56283964189419], 6);       
    
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZmFqYXJzaWRpeCIsImEiOiJja3YwOTdja2gyd3g5MndvZDBieWRhZWd6In0.G5umEESgvVMSCock5qjcgA', {
          attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 18,          
          zoom: 10,
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
          accessToken: 'pk.eyJ1IjoiZmFqYXJzaWRpeCIsImEiOiJja3YwOTdja2gyd3g5MndvZDBieWRhZWd6In0.G5umEESgvVMSCock5qjcgA'
        }).addTo(mymap);   
        
        var latLong = [
            {
                "location": "Aceh",
                "lat": 4.240607212590768, 
                "lng": 96.80985705510021,
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            },
            {
                "location": "Medan",
                "lat": 3.7306696434184095, 
                "lng": 98.35774718699001, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            },  
            {
                "location": "Palembang",
                "lat": -2.9563958593514754, 
                "lng": 104.82182810724744, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "Jakarta",
                "lat": -6.33791589965942, 
                "lng": 106.78444685106263, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "Pontianak",
                "lat": 0.022626699447383304, 
                "lng": 109.30683069143113, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "Balikpapan",
                "lat": -1.227231776594553, 
                "lng": 116.87915739475685, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "Palu",
                "lat": -0.8945514512476466, 
                "lng": 119.86950173244018, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "Makassar",
                "lat": -5.225383740209136, 
                "lng": 119.49699690933481, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "Maluku",
                "lat": -3.3693380735596494, 
                "lng": 130.12528159727233, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "Fak Fak",
                "lat": -2.9193600165778757, 
                "lng": 132.29106707160895, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }, 
            {
                "location": "NTT",
                "lat": -8.55728779037411, 
                "lng": 120.99900206413828, 
                "date": "Desember 2021",
                "cases": "20000",
                "attackers": "China"
            }       
                 
          ]

          latLong.forEach(function (coord) {
            
            
            var radius = coord.cases;
            /*
            if (coord.cases < 10) {
              radius = coord.cases*3000
            } else if (coord.cases < 50) {
              radius = coord.cases*2000
            } else {
              radius = coord.cases*700
            }
            */
  
            var circle = L.circle(coord, {
              color: '#ec2323',
              fillColor: '#ec2323',
              fillOpacity: 0.8,
              borderColor: 'red',
              weight: 5,
              opacity: 0.25,
              radius: radius,
            }).addTo(mymap);
  
            circle.bindPopup('<div class="marker_info"><div class="location">' + coord.location + '</div><p><span class="text-primary">' + coord.date + '</span></p> <p>Kasus: <span class="text-primary font-semibold">' + coord.cases + '</span></p> <p>Penyerang: <span class="font-semibold text-primary">' + coord.attackers + '</span></p></div>');
           
  
          });


        
});