$(document).on("change", "input[type='checkbox']", function () {
	$(this).parent()[this.checked ? "addClass" : "removeClass"]("checked");
});

$(document).ready(function(){
    //isotop
	  var $grid = $('.grid').isotope({
		
	  });
    $('.checkbox').change(function() {        
        if($(this).is(":checked")) {
            var filterValue = $(this).attr('data-filter');
            $grid.isotope({ filter: filterValue });			
        }         
    }); 

});