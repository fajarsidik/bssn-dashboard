$(document).ready(function(){

    $('.modal-btn').each(function(){

        $(this).click(function(){
            var href = $(this).attr('modal');
            var body = $('body')
            
            if(body.hasClass('modal-active')){
                body.removeClass('modal-active')
                $('#' + href).removeClass('modal-show');
            }else{
                body.addClass('modal-active')
                $('#' + href).addClass('modal-show');
            }

            $('.modal-show .close').click(function(){
                body.removeClass('modal-active')
                $('#' + href).removeClass('modal-show');
            })
        })
        
    })

    
});