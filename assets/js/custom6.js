$(document).ready(function(){
    

   //chart 2        
   const ctx = document.getElementById('chart5').getContext('2d');
   const chart_personil = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['Januari','Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
        datasets: [
            {
            label: 'ASN ',
            data: [13,13,13,13,13,13,13, 13, 13, 13, 13, 13],
            backgroundColor: '#fbc143',
            hoverBackgroundColor: '#fbc143',
            borderColor: '#fbc143',
            borderWidth: 1,
            pointBackgroundColor: '#fbc143',
            pointHoverBackgroundColor: '#fbc143',
            pointBorderColor: '#fff',
            pointBorderWidth: 2,
            radius: 4,
            fill: {
                target: 'origin',
                above: 'rgba(253, 226, 120, 0.23)',   // Area will be red above the origin
                below: 'rgba(253, 226, 120, 0.23)'    // And blue below the origin
              }
        },
        {
             label: 'PPPK & PPNPN ',
             data: [122,122,122,122,122,122,122, 122, 122, 122, 122, 122],
             backgroundColor: '#4286d8',
             hoverBackgroundColor: '#4286d8',
             borderColor: '#4286d8',
             borderWidth: 1,
             pointBackgroundColor: '#4286d8',
             pointHoverBackgroundColor: '#4286d8',
             pointBorderColor: '#fff',
             pointBorderWidth: 2,
             radius: 4,
             fill: {
                 target: 'origin',
                 above: 'rgba(66, 134, 216, 0.23)',   // Area will be red above the origin
                 below: 'rgba(66, 134, 216, 0.23)'    // And blue below the origin
             }
         },
         {
             label: 'TNI ',
             data: [65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65],
             backgroundColor: '#e51633',
             hoverBackgroundColor: '#e51633',
             borderColor: '#e51633',
             borderWidth: 1,
             pointBackgroundColor: '#e51633',
             pointHoverBackgroundColor: '#e51633',
             pointBorderColor: '#fff',
             pointBorderWidth: 2,
             radius: 4,
             fill: {
                 target: 'origin',
                 above: 'rgba(229, 22, 51, 0.23)',   // Area will be red above the origin
                 below: 'rgba(229, 22, 51, 0.23)'    // And blue below the origin
             }
         },
         {
             label: 'POLRI ',
             data: [344, 344, 344, 344, 344, 344, 344, 344, 344, 344, 344, 344],
             backgroundColor: '#00c461',
             hoverBackgroundColor: '#00c461',
             borderColor: '#00c461',
             borderWidth: 1,
             pointBackgroundColor: '#00c461',
             pointHoverBackgroundColor: '#00c461',
             pointBorderColor: '#fff',
             pointBorderWidth: 2,
             radius: 4,
             fill: {
                 target: 'origin',
                 above: 'rgba(0, 196, 97, 0.23)',   // Area will be red above the origin
                 below: 'rgba(0, 196, 97, 0.23)'    // And blue below the origin
             }
         },
         {
             label: 'STSN ',
             data: [45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45, 45],
             backgroundColor: '#a156ff',
             hoverBackgroundColor: '#a156ff',
             borderColor: '#a156ff',
             borderWidth: 1,
             pointBackgroundColor: '#a156ff',
             pointHoverBackgroundColor: '#a156ff',
             pointBorderColor: '#fff',
             pointBorderWidth: 2,
             radius: 4,
             fill: {
                 target: 'origin',
                 above: 'rgba(240, 248, 255, 0.23)',   // Area will be red above the origin
                 below: 'rgba(240, 248, 255, 0.23)'    // And blue below the origin
             }
         }
     ]
    },
    options: {
       
        interaction: {
          intersect: false,
        },
        scales: {
            x: {
                grid: {
                display: false
                },
                title: {
                display: false
                },
            },
            y: {
                title: {
                display: false
                },
                grid: {
                display: true
                },

            }
        },
      },
   });

   //pie chart
   const ctx_pie = document.getElementById('pie-chart').getContext('2d');
   const data = {
    labels: ['TNI', 'POLRI', 'PPPK & PPNPN', 'ASN', 'STSN'],
    datasets: [
      {
        label: 'TOTAL PERSONIL',
        data: [30, 20, 20, 10, 30],
        backgroundColor: ['#ffc300', '#a156ff', '#e51633', '#4286d8','#00c461']        
      }
    ]
  };
   const myChart2 = new Chart(ctx_pie, {
    type: 'doughnut',
    data: data,
    options: {
        aspectRatio: 2,
        responsive: true,
        plugins: {
        legend: {
            display: true,
            position: 'right',
        },
        title: {
            display: false,
            text: 'Chart.js Pie Chart'
        }
        }
    },
   });

   var json = JSON.parse($.ajax({'url': "./assets/js/data/data-asn.json", 'async': false}).responseText); 
   console.log(json)
    var labels = json.map(function(e) {
        return e.type;
     });

     var dataasn = json.map(function(e) {
        return e.value;
     });
   //chart asn
   const ctx_asn = document.getElementById('chart-asn').getContext('2d');
        const myChart_asn = new Chart(ctx_asn, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'ASN ',
                    data: dataasn,
                    backgroundColor: '#263053',
                    hoverBackgroundColor: '#ff9d50',
                    borderColor: '#fff',
                    borderWidth: 1
                }]
            },
            options: {
                aspectRatio: 2.15,
                responsive: true,
                plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Chart.js Bar Chart'
                }
                },
                scales: {
                    x: {
                        grid: {
                        display: false
                        },
                        title: {
                        display: false
                        },
                    },
                    y: {
                        title: {
                        display: false
                        },
                        grid: {
                        display: true
                        },

                    }
                },
            }
        });


        //chart tni
            const ctx_tni = document.getElementById('chart-tni').getContext('2d');
            const myChart_tni = new Chart(ctx_tni, {
                type: 'bar',
                data: {
                    labels: ['Bintara', 'Perwira Pertama', 'Perwira Menengah', 'Perwira Tinggi'],
                    datasets: [{
                        label: 'TNI ',
                        data: [20, 11, 10, 1],
                        backgroundColor: '#263053',
                        hoverBackgroundColor: '#ff9d50',
                        borderColor: '#fff',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart'
                    }
                    },
                    scales: {
                        x: {
                            grid: {
                            display: false
                            },
                            title: {
                            display: false
                            },
                        },
                        y: {
                            title: {
                            display: false
                            },
                            grid: {
                            display: true
                            },

                        }
                    },
                }
            });

        //chart pppk
        const ctx_pppk = document.getElementById('chart-pppk').getContext('2d');
        const myChart_pppk = new Chart(ctx_pppk, {
            type: 'bar',
            data: {
                labels: ['Golongan I', 'Golongan II', 'Golongan III', 'Golongan IV'],
                datasets: [{
                    label: 'PPPK & PPNPN ',
                    data: [ 1000, 400, 10, 5],
                    backgroundColor: '#263053',
                    hoverBackgroundColor: '#ff9d50',
                    borderColor: '#fff',
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                plugins: {
                legend: {
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Chart.js Bar Chart'
                }
                },
                scales: {
                    x: {
                        grid: {
                        display: false
                        },
                        title: {
                        display: false
                        },
                    },
                    y: {
                        title: {
                        display: false
                        },
                        grid: {
                        display: true
                        },

                    }
                },
            }
        });  
        
    //chart stsn
    const ctx_stsn = document.getElementById('chart-stsn').getContext('2d');
    const myChart_stsn = new Chart(ctx_stsn, {
        type: 'bar',
        data: {
            labels: ['Golongan I', 'Golongan II', 'Golongan III', 'Golongan IV'],
            datasets: [{
                label: 'STSN',
                data: [ 1000, 400, 10, 5],
                backgroundColor: '#263053',
                hoverBackgroundColor: '#ff9d50',
                borderColor: '#fff',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: 'Chart.js Bar Chart'
            }
            },
            scales: {
                x: {
                    grid: {
                    display: false
                    },
                    title: {
                    display: false
                    },
                },
                y: {
                    title: {
                    display: false
                    },
                    grid: {
                    display: true
                    },

                }
            },
        }
    }); 
    
    //chart polri
    const ctx_polri = document.getElementById('chart-polri').getContext('2d');
    const myChart_polri = new Chart(ctx_polri, {
        type: 'bar',
        data: {
            labels: ['Bripka', 'Brigpol', 'Briptu', 'Bripda'],
            datasets: [{
                label: 'POLRI',
                data: [ 1000, 400, 10, 5],
                backgroundColor: '#263053',
                hoverBackgroundColor: '#ff9d50',
                borderColor: '#fff',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: false,
                text: 'Chart.js Bar Chart'
            }
            },
            scales: {
                x: {
                    grid: {
                    display: false
                    },
                    title: {
                    display: false
                    },
                },
                y: {
                    title: {
                    display: false
                    },
                    grid: {
                    display: true
                    },

                }
            },
        }
    }); 
});