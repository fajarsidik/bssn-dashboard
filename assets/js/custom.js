$(document).ready(function(){
    
    // MAP
    var mymap = L.map('maps').setView([17.093911, 14.678468], 2);       
    
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZmFqYXJzaWRpeCIsImEiOiJja3YwOTdja2gyd3g5MndvZDBieWRhZWd6In0.G5umEESgvVMSCock5qjcgA', {
          attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 18,          
          zoom: 10,
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
          accessToken: 'pk.eyJ1IjoiZmFqYXJzaWRpeCIsImEiOiJja3YwOTdja2gyd3g5MndvZDBieWRhZWd6In0.G5umEESgvVMSCock5qjcgA'
        }).addTo(mymap);   
        
        var latLong = [
            {
              "location": "Nigeria",
              "lat": 18.216307,
              "lng": 11.884072,
              "date": "Desember 2021",
              "cases": "259729",
              "attackers": "China"
            },
            {
                "location": "Mozambique",
                "lat": -20.233852,
                "lng": 31.408819,
                "date": "Januari 2021",
                "cases": "159729",
                "attackers": "Rusia"
            },
            {
                "location": "India",
                "lat": 19.986104,
                "lng": 78.670715,
                "date": "Februari 2021",
                "cases": "199729",
                "attackers": "Canada"
            },
            {
                "location": "China",
                "lat": 36.777513,
                "lng": 98.719264,
                "date": "Februari 2021",
                "cases": "299729",
                "attackers": "Canada"
            },
            {
                "location": "Japan",
                "lat": 36.204823,
                "lng": 138.252930,
                "date": "Maret 2021",
                "cases": "99729",
                "attackers": "Canada"
            } ,  
            {
                "location": "Singapore",
                "lat": 0.936390,
                "lng": 104.227617,
                "date": "Juli 2021",
                "cases": "209729",
                "attackers": "Canada"
            }, 
            {
                "location": "Philippines",
                "lat": 12.713662,
                "lng": 122.664243,
                "date": "Juli 2021",
                "cases": "109729",
                "attackers": "China"
            }, 
            {
                "location": "USA",
                "lat": 38.320111,
                "lng": -99.952862,
                "date": "Juli 2021",
                "cases": "509729",
                "attackers": "China"
            }                                                      

          ]

          latLong.forEach(function (coord) {
            
            
            var radius = coord.cases;
            /*
            if (coord.cases < 10) {
              radius = coord.cases*3000
            } else if (coord.cases < 50) {
              radius = coord.cases*2000
            } else {
              radius = coord.cases*700
            }
            */
  
            var circle = L.circle(coord, {
              color: '#ec2323',
              fillColor: '#ec2323',
              fillOpacity: 0.8,
              borderColor: 'red',
              weight: 10,
              opacity: 0.25,
              radius: radius,
            }).addTo(mymap);
  
            circle.bindPopup('<div class="marker_info"><div class="location">' + coord.location + '</div><p><span class="text-primary">' + coord.date + '</span></p> <p>Kasus: <span class="text-primary font-semibold">' + coord.cases + '</span></p> <p>Penyerang: <span class="font-semibold text-primary">' + coord.attackers + '</span></p></div>');
           
  
          });

          //chart 1   
          /*       
          const ctx = document.getElementById('chart1').getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Rusia', 'America', 'India', 'Arab', 'Singapore', 'Indonesia', 'Turki', 'London', 'China'],
                    datasets: [{
                        label: 'Serangan ',
                        data: [1200, 2190, 3000, 5000, 2122, 3019, 3019, 1019, 8019],
                        backgroundColor: '#b2b6c7',
                        hoverBackgroundColor: '#ff9d50',
                        borderColor: '#fff',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    plugins: {
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart'
                    }
                    },
                    scales: {
                        x: {
                            grid: {
                            display: false
                            },
                            title: {
                            display: false
                            },
                        },
                        y: {
                            title: {
                            display: false
                            },
                            grid: {
                            display: true
                            },

                        }
                    },
                }
            });

        //chart 2        
        
        const ctx2 = document.getElementById('chart2').getContext('2d');
        const myChart2 = new Chart(ctx2, {
            type: 'line',
            data: {
                labels: ['Rusia', 'America', 'India', 'Arab', 'Singapore', 'Indonesia', 'Turki', 'London', 'China'],
                datasets: [{
                    label: 'Serangan ',
                    data: [1200, 2190, 3000, 5000, 2122, 3019, 3019, 1019, 8019],
                    backgroundColor: '#b2b6c7',
                    hoverBackgroundColor: '#ff9d50',
                    borderColor: '#fbc143',
                    borderWidth: 4,
                    fill: {
                        target: 'origin',
                        above: 'rgba(253, 226, 120, 0.23)',   // Area will be red above the origin
                        below: 'rgba(253, 226, 120, 0.23)'    // And blue below the origin
                      }
                }]
            },
            options: {
               backgroundColor: '#000',
                pointBackgroundColor: '#fbc143',
                pointHoverBackgroundColor: '#ff0002',
                pointBorderColor: '#fff',
                pointBorderWidth: 2,
                radius: 7,
                interaction: {
                  intersect: false,
                },
                scales: {
                    x: {
                        grid: {
                        display: false
                        },
                        title: {
                        display: false
                        },
                    },
                    y: {
                        title: {
                        display: false
                        },
                        grid: {
                        display: true
                        },

                    }
                },
              },
        });
        */

        //chart pagu
        const ctx_pie = document.getElementById('pagu-chart').getContext('2d');
        const data = {
            labels: ['PAGU', 'PENYERAPAN'],
            datasets: [
            {
                label: 'PENYERAPAN ANGGARAN 2022',
                data: [99, 1],
                backgroundColor: ['#263053', '#ffc300']        
            }
            ]
        };
        const pagu_chart = new Chart(ctx_pie, {
            type: 'doughnut',
            data: data,
            options: {
                weight: 1,
                responsive: true,
                plugins: {
                legend: {
                    display: false,
                    position: 'bottom',
                },
                title: {
                    display: false,
                    text: 'Chart.js Pie Chart'
                }
                }
            },
           });

        //chart siber
        const ctx_pie2 = document.getElementById('siber-chart').getContext('2d');
        const data2 = {
            labels: ['Rusia', 'China', 'Amerika', 'Canada', 'Australia'],
            datasets: [
            {
                label: 'PENYERAPAN ANGGARAN 2022',
                data: [14.1, 13.6, 12.8, 5.3, 53.9],
                backgroundColor: ['#00c461', '#ffc300', '#a156ff' ,'#2272d2','#e51633']        
            }
            ]
        };
        const siber_chart = new Chart(ctx_pie2, {
            type: 'doughnut',
            data: data2,
            options: {
                aspectRatio: 2,
                responsive: true,
                plugins: {
                legend: {
                    display: true,
                    position: 'right',
                },
                title: {
                    display: false,
                    text: 'Chart.js Pie Chart'
                }
                }
            },
           });

    //chart 2        
   const ctx_personil = document.getElementById('personil-chart').getContext('2d');
   const chart_personil = new Chart(ctx_personil, {
       type: 'line',
       data: {
           labels: ['JUL', 'AGS', 'SEP', 'OKT', 'NOV', 'DES'],
           datasets: [
               {
               label: 'ASN ',
               data: [13, 13, 13, 13, 13, 13],
               backgroundColor: '#fbc143',
               hoverBackgroundColor: '#fbc143',
               borderColor: '#fbc143',
               borderWidth: 1,
               pointBackgroundColor: '#fbc143',
               pointHoverBackgroundColor: '#fbc143',
               pointBorderColor: '#fff',
               pointBorderWidth: 2,
               radius: 4,
               fill: {
                   target: 'origin',
                   above: 'rgba(253, 226, 120, 0.23)',   // Area will be red above the origin
                   below: 'rgba(253, 226, 120, 0.23)'    // And blue below the origin
                 }
           },
           {
                label: 'PPPK & PPNPN ',
                data: [122, 122, 122, 122, 122, 122],
                backgroundColor: '#4286d8',
                hoverBackgroundColor: '#4286d8',
                borderColor: '#4286d8',
                borderWidth: 1,
                pointBackgroundColor: '#4286d8',
                pointHoverBackgroundColor: '#4286d8',
                pointBorderColor: '#fff',
                pointBorderWidth: 2,
                radius: 4,
                fill: {
                    target: 'origin',
                    above: 'rgba(66, 134, 216, 0.23)',   // Area will be red above the origin
                    below: 'rgba(66, 134, 216, 0.23)'    // And blue below the origin
                }
            },
            {
                label: 'TNI ',
                data: [65, 65, 65, 65, 65, 65],
                backgroundColor: '#e51633',
                hoverBackgroundColor: '#e51633',
                borderColor: '#e51633',
                borderWidth: 1,
                pointBackgroundColor: '#e51633',
                pointHoverBackgroundColor: '#e51633',
                pointBorderColor: '#fff',
                pointBorderWidth: 2,
                radius: 4,
                fill: {
                    target: 'origin',
                    above: 'rgba(229, 22, 51, 0.23)',   // Area will be red above the origin
                    below: 'rgba(229, 22, 51, 0.23)'    // And blue below the origin
                }
            },
            {
                label: 'POLRI ',
                data: [344, 344, 344, 344, 344, 344],
                backgroundColor: '#00c461',
                hoverBackgroundColor: '#00c461',
                borderColor: '#00c461',
                borderWidth: 1,
                pointBackgroundColor: '#00c461',
                pointHoverBackgroundColor: '#00c461',
                pointBorderColor: '#fff',
                pointBorderWidth: 2,
                radius: 4,
                fill: {
                    target: 'origin',
                    above: 'rgba(0, 196, 97, 0.23)',   // Area will be red above the origin
                    below: 'rgba(0, 196, 97, 0.23)'    // And blue below the origin
                }
            },
            {
                label: 'STSN ',
                data: [45, 45, 45, 45, 45, 45],
                backgroundColor: '#a156ff',
                hoverBackgroundColor: '#a156ff',
                borderColor: '#a156ff',
                borderWidth: 1,
                pointBackgroundColor: '#a156ff',
                pointHoverBackgroundColor: '#a156ff',
                pointBorderColor: '#fff',
                pointBorderWidth: 2,
                radius: 4,
                fill: {
                    target: 'origin',
                    above: 'rgba(240, 248, 255, 0.23)',   // Area will be red above the origin
                    below: 'rgba(240, 248, 255, 0.23)'    // And blue below the origin
                }
            }
        ]
       },
       options: {
          
           interaction: {
             intersect: false,
           },
           scales: {
               x: {
                   grid: {
                   display: false
                   },
                   title: {
                   display: false
                   },
               },
               y: {
                   title: {
                   display: false
                   },
                   grid: {
                   display: true
                   },

               }
           },
         },
   });

   const ctx_covid = document.getElementById('covid-chart').getContext('2d');
   const chart_covid = new Chart(ctx_covid, {
       type: 'line',
       data: {
           labels: ['JUL', 'AGS', 'SEP', 'OKT', 'NOV', 'DES'],
           datasets: [
               {
               label: 'ASN ',
               data: [1, 2, 3, 10, 2, 0],
               backgroundColor: '#fbc143',
               hoverBackgroundColor: '#fbc143',
               borderColor: '#fbc143',
               borderWidth: 1,
               pointBackgroundColor: '#fbc143',
               pointHoverBackgroundColor: '#fbc143',
               pointBorderColor: '#fff',
               pointBorderWidth: 2,
               radius: 4,
               fill: {
                   target: 'origin',
                   above: 'rgba(253, 226, 120, 0.23)',   // Area will be red above the origin
                   below: 'rgba(253, 226, 120, 0.23)'    // And blue below the origin
                 }
           }
        ]
       },
       options: {
          
           interaction: {
             intersect: false,
           },
           scales: {
               x: {
                   grid: {
                   display: false
                   },
                   title: {
                   display: false
                   },
               },
               y: {
                   title: {
                   display: false
                   },
                   grid: {
                   display: true
                   },

               }
           },
         },
   });
});